# Add CPU Scheduling Algorithms to XV6 (Spring 2019)
- Written in C, based on XV6 project, Operating Systems course project, Dr. Nastooh Taheri Javan
- Including Round Robin, FIFO Round Robin, Guaranteed (Fair-Share) scheduling, Multilevel queue scheduling
Algorithms